<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to(url('/').'/admin');
})->name('HomeToLogin');

Route::middleware('login')->group(function () {
  Route::get('/project/detail/{id}',[
    'uses' => 'ProjectController@detail',
    'as' => 'ProjectDetail'
  ]);

  Route::post('/project/edit/{id}',[
    'uses' => 'ProjectController@edit',
    'as' => 'ProjectEdit'
  ]);

  Route::get('/project/done/{id}',[
    'uses' => 'ProjectController@done',
    'as' => 'ProjectDone'
  ]);

  Route::get('/calendar',[
    'uses' => 'ProjectController@calendar',
    'as' => 'ProjectCalendar'
  ]);

  Route::post('/post/new',[
    'uses' => 'ProjectController@new_post_calendar',
    'as' => 'ProjectPostCalendar'
  ]);

  Route::get('/calendar/edit/{id}',[
    'uses' => 'ProjectController@edit_post_calendar',
    'as' => 'ProjectEditPostCalendar'
  ]);

  Route::post('/calendar/comment/{id}',[
    'uses' => 'ProjectController@post_calendar_comment',
    'as' => 'postComment'
  ]);

  Route::get('/calendar/approval/producer/{id}','ProjectController@approval_producer')->name('approvalProducer');
  Route::get('/calendar/approval/editor/{id}','ProjectController@approval_editor')->name('approvalEditor');
  Route::get('/calendar/approval/client/{id}','ProjectController@approval_client')->name('approvalClient');

  Route::get('/approval-post/platform/{id}/{type}','ProjectController@approval_satuan')->name('approvalSatuan');

  Route::get('/post/delete/{id}',[
    'uses' => 'ProjectController@delete_post_calendar',
    'as' => 'ProjectDeletePostCalendar'
  ]);

  Route::post('/post/editing/{id}',[
    'uses' => 'ProjectController@editing_post_calendar',
    'as' => 'ProjectEditingPostCalendar'
  ]);

  Route::post('/post/comment/gambar/{id}',[
    'uses' => 'ProjectController@comment_gambar',
    'as' => 'ProjectCommentImageCalendar'
  ]);

  Route::get('/status-post/platform/{id}/{type}/{status}','ProjectController@postToStatus')->name('editPostTo');

  Route::get('/post/image-display/{id}/{gbr}','ProjectController@imageDelete')->name('postImgDelete');
  Route::post('/post/add-display/{id}','ProjectController@displayAdd')->name('postAddDisplay');
  Route::post('/post/add-video/{id}','ProjectController@videoAdd')->name('postAddVideo');
});


route::get('/lab/test',[
  'uses' => 'ProjectController@lab',
  'as' => 'ProjectTest'
]);
