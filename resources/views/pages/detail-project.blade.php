@extends("crudbooster::admin_template")

@section('csslineup')
  <link rel="stylesheet" href="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">

@endsection

@section("content")
  <div class="row">
    <div class="col-md-6">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          {{$project->nama_project}}
        </div>
        <!-- /.box-body -->
        <div class="box-body">
          <img src="{{url('/')}}/{{$project->project_img}}" width="100%" alt="">
        </div>
      </div>
      <!-- /.box -->

      <!-- About Me Box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Project's Profile</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! nl2br($project->project_desc) !!}
        </div>
        <!-- /.box-body -->
      </div>



      <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">To Do List</h3>

              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <ul class="todo-list">
                @foreach($todolist as $todo)
                  <?php
                    switch($todo->job_status){
                      case 'new' : $color = 'primary';break;
                      case 'progress' : $color = 'primary';break;
                      case 'check' : $color = 'warning';break;
                      case 'reject' : $color = 'danger';break;
                      case 'revision' : $color = 'warning';break;
                      case 'done' : $color = 'success';break;
                    }

                    $todo_user = DB::table('cms_users')->where('id',$todo->user_id)->first();
                  ?>
                  <li class="{{$todo->job_status == 'done' ? 'done' : ''}}">
                    <!-- drag handle -->
                    <span class="handle">
                          <i class="fa fa-ellipsis-v"></i>
                          <i class="fa fa-ellipsis-v"></i>
                        </span>
                    <!-- checkbox -->
                    @if($todo->job_status != 'done' && $todo->user_id == Crudbooster::myID())
                      <input type="checkbox"  onclick='window.location.assign("{{route('ProjectDone',$todo->id)}}")'/>
                    @endif
                    <!-- todo text -->
                    <small>{{$todo_user->name}}</small>

                    <span class="text">{{$todo->todo}}</span>
                    <!-- Emphasis label -->
                    <small class="label label-{{$color}}"><i class="fa fa-clock-o"></i> {{$todo->job_status}}</small>
                    <!-- General tools such as edit or delete-->
                    <div class="tools">
                      <a href="{{url('/')}}/admin/todolist/edit/4?return_url={{url('/')}}%2Fproject%2Fdetail%2F{{$todo->id}}&parent_id=&parent_field="><i class="fa fa-edit"></i></a>
                      <i class="fa fa-trash-o"></i>
                    </div>
                  </li>
                @endforeach

              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <a href="{{url('/')}}/admin/todolist/add?return_url={{url('/')}}%2Fproject%2Fdetail%2F{{$id}}&parent_id=&parent_field=" type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</a>
            </div>
          </div>
          <!-- /.box -->
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-6">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#timeline" data-toggle="tab">Info Terupdate</a></li>
        </ul>
        <div class="tab-content">

          <div class="active tab-pane" id="timeline">
            <p><b>Kirim Informasi :</b></p>
            <form class="form-horizontal" method="post" action="{{ route('ProjectEdit',$project->id) }}"  enctype="multipart/form-data">
              <div class="form-group">
                <div class="col-sm-12">
                  <textarea name="description" rows="2" class="form-control" required></textarea>
                </div>
              </div>
              <div class="form-group">

                <div class="col-sm-6">

                  <label>Deadline</label>
                  <input class="form-control" type="date" name="deadline" required>
                </div>

                <div class="col-sm-6">
                  <label>Assign To</label>
                  <select class="form-control" class="" name="assign" required>
                    <option value="">--assign to --</option>
                  @foreach($teams as $n => $team)
                    <option value="{{$team->id}}">{{$team->name}}</option>
                  @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6">
                  <input type="file" name="bukti" class="form-control">
                </div>
              </div>
              {{csrf_field()}}
              <input type="hidden" name="project_id" value="{{$project->id}}">
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success pull-right">Submit</button>
                </div>
              </div>
            </form>

            <!-- The timeline -->

            <ul class="timeline timeline-inverse">
              @foreach($activities as $activity)
                <?php
                  $to = DB::table('cms_users')->where('id',$activity->user_id)->first();
                  $from = DB::table('cms_users')->where('id',$activity->from_id)->first();
                ?>
                <!-- timeline time label -->

                <li class="time-label">
                  <span class="bg-blue">
                    {{ date('d-m-Y',strtotime($activity->created_at))}}
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa fa-user bg-aqua"></i>

                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> {{ $activity->created_at ? $activity->created_at : '-' }}</span>

                    <h3 class="timeline-header"><a href="#">{{ ucwords($to->name) }}</a> </h3>

                    <div class="timeline-body">
                      @if($activity->image)
                        <img src="{{url('/')}}/uploads/{{$activity->image}}" alt="" width="80%">
                      @endif
                      <br>
                      {!! nl2br($activity->description) !!}

                    </div>
                    <div class="timeline-footer">
                      deadline : {{ $activity->deadline_at }} from {{ucwords($from->name)}}
                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              @endforeach

            </ul>
          </div>
          <!-- /.tab-pane -->

        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
@endsection

@section('jslineup')
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/knob/jquery.knob.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>

  <script src="{{url('/')}}/vendor/crudbooster/assets/adminlte/dist/js/pages/dashboard.js" type="text/javascript"></script>


@endsection

@section('jsonpage')

<script type="text/javascript">

  function clickToChange(x){

  }


</script>

@endsection
